\ProvidesClass{tudo-sse-thesis}[2021/10/05 v1.0 TU Dortmund Secure Software Engineering Group Thesis Template]
\NeedsTeXFormat{LaTeX2e}
\LoadClass[titlepage=firstiscover, enabledeprecatedfontcommands]{scrbook}

% Force XeLaTeX
\RequirePackage{ifxetex}
\RequireXeTeX

\RequirePackage{graphicx}
\RequirePackage{url}
\RequirePackage{geometry}
\RequirePackage{xifthen}
\RequirePackage{wrapfig}
\RequirePackage{textpos}
\RequirePackage{multicol}

% BibLaTex settings
\RequirePackage[ngerman]{babel} % change this to english if necessary
\RequirePackage[babel]{csquotes}
\RequirePackage[style=numeric, giveninits=true, maxbibnames=99, isbn=false]{biblatex}
\renewcommand*{\mkbibcompletename}[1]{\textsc{#1}}
\DefineBibliographyStrings{ngerman}{
   andothers = {{et\,al\adddot}},
}
\DeclareNameAlias{default}{family-given}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{xcolor}
\RequirePackage[quiet]{fontspec}
\RequirePackage{pdfpages}

%%% Custom Stuff %%%
\RequirePackage[onehalfspacing]{setspace}  %Line space = 1.5
\RequirePackage{dirtytalk} % use \say{} for "double quotes"
\RequirePackage{ragged2e}
\RequirePackage{float} % control floating for environments like figure
\RequirePackage[shortlabels]{enumitem} % allow shortlabes for enumeration
\RequirePackage{hyperref} % makes refs and links clickable
\RequirePackage{indentfirst} % indent at beginning of every chapter/ section etc
\RequirePackage{booktabs}  % academic style for tables
\RequirePackage{multirow} % also for tables
\RequirePackage[]{tikz} % latex graphics
\RequirePackage{braket} % includes the \set{} command for sets like {1,2,3}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\PassOptionsToPackage{hyphens}{url}
\defaultfontfeatures{Ligatures=TeX}
%\let\cleardoublepage\clearpage
% Font Configuration
%\RequirePackage{libertineRoman}
%\RequirePackage{nimbussans}

\setsansfont{NotoSans}[
	  Ligatures={Common,Rare},
		Path=./fonts/NotoSans/,
		Scale=1.0,
		Extension = .ttf,
		UprightFont=*-Regular,
		BoldFont=*-Bold,
		ItalicFont=*-Italic,
		BoldItalicFont=*-BoldItalic]

\setmonofont{Inconsolata}[
				Path=./fonts/Inconsolata/,
				Scale=1.0,
				Extension = .ttf,
				UprightFont=*-Regular,
				BoldFont=*-Bold]

\setmainfont{LinLibertine}%
   [Numbers=OldStyle,
	 Path=./fonts/LinLibertine/,
	 Scale=1.0,
	 Extension = .otf,
	 UprightFont=*_R,
	 BoldFont=*_RB,
	 ItalicFont=*_RI,
	 BoldItalicFont=*_RBI]

\RequirePackage[libertine]{newtxmath}

\setkomafont{pagehead}{\sffamily}
\setkomafont{pagenumber}{\sffamily}
\setkomafont{footnote}{\rmfamily}
\setkomafont{caption}{\rmfamily}
\setkomafont{captionlabel}{\rmfamily}

% Listings
\RequirePackage{listings}
\definecolor{KWColor}{rgb}{0.37,0.08,0.25}
\definecolor{CommentColor}{rgb}{0.133,0.545,0.133}
\definecolor{StringColor}{rgb}{0,0.126,0.941}
\lstset{
		language=Java,
    float=th,
        columns=fullflexible,
        basicstyle=\scriptsize\ttfamily,
        numbers=left,
        numberstyle=\tiny\color{black},
        captionpos=b,
        xleftmargin=15pt,
	      tabsize=4,
        numbersep=5pt,
      	stepnumber=1,
        extendedchars=true,
        breaklines=true,
        breakatwhitespace=true,
        showtabs=false,
        showspaces=false,
        showstringspaces=false,
        keywordstyle=\bfseries\color{KWColor},
        commentstyle=\color{CommentColor},
        stringstyle=\color{StringColor},
        escapechar=\%,
 		language=Java,
		escapeinside={(*@}{@*)}
}


% Internationalization
\newcommand{\thesistype}{}
\newcommand{\examination}{}

\DeclareOption{german}{%
  \PassOptionsToPackage{main=german,english}{babel}
  \DeclareOption{ba}{\renewcommand{\thesistype}{Bachelorarbeit}}
  \DeclareOption{ma}{\renewcommand{\thesistype}{Masterarbeit}}
  \DeclareOption{expose}{\renewcommand{\thesistype}{Exposé zur Bachelorarbeit}}
  \renewcommand{\examination}{Begutachtung:}
}
\DeclareOption{english}{%
  \PassOptionsToPackage{main=english,german}{babel}
  \DeclareOption{ba}{\renewcommand{\thesistype}{Bachelor Thesis}}
  \DeclareOption{ma}{\renewcommand{\thesistype}{Master Thesis}}
  \renewcommand{\examination}{Reviewer:}
}

\DeclareOption*{\PackageWarning{tudo-sse-thesis}{Unknown ‘\CurrentOption’}}
\ExecuteOptions{english,ba}
\ProcessOptions\relax

% This needs to be added for multilanguage support...
\defcaptionname{english}\abstractname{Abstract}
\defcaptionname{german}\abstractname{Zusammenfassung}

% Environments for English and German abstracts
\newenvironment{abstract}
  {\begin{otherlanguage}{english}\chapter*{\abstractname}
  \addcontentsline{toc}{chapter}{\abstractname}
  }
  {
  \end{otherlanguage}
  }

\newenvironment{abstract-ger}
  {\begin{otherlanguage}{german}\chapter*{\abstractname}
  \addcontentsline{toc}{chapter}{\abstractname}
  }
  {
  \end{otherlanguage}
  }

% Title page
\newcommand\firstexaminer[1]{\renewcommand\@firstexaminer{#1}}
\newcommand\@firstexaminer{JProf.\ Dr.-Ing.\ Ben Hermann}

\newcommand\secondexaminer[1]{\renewcommand\@secondexaminer{#1}}
\newcommand\@secondexaminer{\@latex@error{No \noexpand\secondexaminer given}\@ehc}


\renewcommand*{\coverpagetopmargin}{100mm}
\renewcommand*{\coverpageleftmargin}{100mm}
\renewcommand*{\coverpagerightmargin}{0mm}
\renewcommand*{\coverpagebottommargin}{0mm}



\renewcommand*{\maketitle}[1][1]{

  \newgeometry{top=4cm,outer=3cm,inner=3cm,bottom=5cm}
  \begin{titlepage}
    \cleardoublepage
    \pagestyle{empty}

    \begin{textblock}{8}(1,11)
      \small \raggedright \noindent \sffamily
      \setlength{\columnsep}{-5.9cm}
      \begin{multicols}{2}
        \includegraphics[width=2cm]{logos/sse_logo.pdf}
        \columnbreak

        Technische Universität Dortmund \\
        Fakultät für Informatik \\
        Lehrstuhl V - Programmiersysteme \\
        Fachgruppe Softwaretechnik sicherer Systeme \\
        \url{https://sse.cs.tu-dortmund.de}
      \end{multicols}
    \end{textblock}

    \centering\sffamily

    \includegraphics[width=15cm]{logos/tud_logo_cmyk.pdf}

    \vspace{0.2cm}

    {\thesistype}

    \vspace{1cm}

    {\Huge\bfseries\@title}

    \vspace{1cm}

    {\Large\@author}

    \vspace{0.5cm}

    {\large\@date}

    \vspace{2cm}

    \examination \\
    \@firstexaminer \\
    \@secondexaminer

    \vfill

    \pagebreak

  \end{titlepage}
  \restoregeometry
}
